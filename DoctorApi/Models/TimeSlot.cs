using NodaTime;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DoctorApi.Models.Time
{
    public enum SlotType
    {
        CLOSED, OPEN, BOOKED, RESTRICTED, OTHER
    }

    public interface ITimeSlot
    {
        LocalTime Time { get; }
        SlotType Type { get; }
    }

    public class TimeSlot
    {
        [BsonId]
        [BsonIgnoreIfDefault]
        [BsonRepresentation(BsonType.ObjectId)]
        public virtual string Id { get; set; }

        [BsonElement("slot")]
        protected virtual int Slot { get; set; }

        [BsonIgnore]
        public virtual LocalTime Time { get => SlotToTime(Slot); }

        [BsonElement("type")]
        public virtual SlotType Type { get; set; }

        public static int TimeToSlot(int hours, int minutes) =>
            hours * 3 + minutes / 20;

        public static LocalTime SlotToTime(int slot) =>
            new LocalTime(slot / 3, slot % 3);
        
        public TimeSlot(int hours, int minutes) =>
            Slot = TimeToSlot(hours, minutes);

        public TimeSlot(LocalTime time) =>
            Slot = TimeToSlot(time.Hour, time.Minute);

        public override bool Equals(object obj)
        {
            if ((obj == null) || ! this.GetType().Equals(obj.GetType())) 
            {
                return false;
            }
            else
            {
                TimeSlot other = (TimeSlot) obj;
                return Slot == other.Slot;
            }
        }

        public override int GetHashCode()
        {
            return Slot.GetHashCode();
        }
    }
}