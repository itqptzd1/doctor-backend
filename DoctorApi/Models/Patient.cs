using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DoctorApi.Models.Users
{
    public class Patient : RawUser, User
    {
        [BsonElement("role")]
        public override string Role { get => Roles.Patient; }

        [BsonElement("name")]
        public PersonsName Name { get; set; }

        [BsonElement("med_id")]
        public string MedId { get; set; }
    }
}
