using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DoctorApi.Models
{
    public class PersonsName
    {
        [BsonElement("firstName")]
        public string FirstName { get; set; }
        [BsonElement("surname")]
        public string Surname { get; set; }
        [BsonElement("patronym")]
        public string Patronym { get; set; }
    }

    public interface DBObject
    {
        [BsonId]
        [BsonIgnoreIfDefault]
        [BsonRepresentation(BsonType.ObjectId)]
        string Id { get; set; }
    }
}