using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using DoctorApi.Models.Time;

namespace DoctorApi.Models.Users
{
    public class Doctor : RawUser, User
    {
        [BsonElement("role")]
        public override string Role { get => Roles.Doctor; }

        [BsonElement("name")]
        public PersonsName Name { get; set; }

        [BsonElement("department")]
        public MongoDBRef Department { get; set; }
        
        [BsonElement("schedule")]
        public DoctorSchedule Schedule { get; set; }
    }
}