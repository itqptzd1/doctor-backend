using System.Collections.Generic;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DoctorApi.Models
{
    public class Department : DBObject
    {
        [BsonId]
        [BsonIgnoreIfDefault]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        // TODO: Switch to MongoDBRef
        [BsonElement("doctors")]
        public List<MongoDBRef> Doctors { get; set; }
    }
}