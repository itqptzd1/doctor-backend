using System.Collections.Generic;
using NodaTime;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DoctorApi.Models.Time
{
    public class Appointment
    {
        public TimeSlot Time { get; set; }

        public MongoDBRef Doctor { get; set; }

        public MongoDBRef Patient { get; set; }

        public string Comment { get; set; }
    }
}