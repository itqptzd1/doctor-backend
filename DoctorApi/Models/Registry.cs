using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DoctorApi.Models.Users
{
    public class Registry : RawUser, User
    {
        [BsonElement("role")]
        public override string Role { get => Roles.Registry; }

        [BsonElement("name")]
        public PersonsName Name { get; set; }
    }
}
