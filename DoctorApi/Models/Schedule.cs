using System.Collections.Generic;
using NodaTime;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DoctorApi.Models.Time
{
    public class DoctorSchedule
    {
        // TODO: delete days that are in the past, saving only the appointments

        [BsonElement("days")]
        public HashSet<IsoDayOfWeek> WorkingDays { get; set; }

        // contains a set of CLOSED slots in which the doctor doesnt accept bookings
        [BsonElement("working_hours")]
        public HashSet<TimeSlot> WorkingHours { get; set; }

        [BsonElement("schedules")]
        public Dictionary<LocalDate, DaySchedule> Days { get; set; }

        public bool addSlot(LocalDate day, TimeSlot slot)
        {
            if(!Days.ContainsKey(day))
            {
                Days.Add(day, new DaySchedule(WorkingHours));
            }

            return Days[day].Slots.Add(slot);
        }

        // TODO: Come up with a better algorithm and var names
        public TimeSlot checkSlot(LocalDate day, int _slot)
        {
            if(Days.ContainsKey(day))
            {
                foreach(var slot in Days[day].Slots)
                {
                    if(TimeSlot.SlotToTime(_slot) == slot.Time)
                    {
                        return slot;
                    }
                }
                return null;
            }
            return null;
        }

        public void deleteSlot(LocalDate day, LocalTime time)
        {
            if(Days.ContainsKey(day))
            {
                Days[day].Slots.RemoveWhere(slot => slot.Time == time);
            }
        }

        public void deleteSlot(LocalDate day, int slot) =>
            deleteSlot(day, TimeSlot.SlotToTime(slot));

        public bool updateSlot(LocalDate day, int slot, TimeSlot upd)
        {
            if(Days.ContainsKey(day))
            {
                deleteSlot(day, slot);
                addSlot(day, upd);

                return true;
            }
            
            return false;
        }

        public void removeOld(LocalDate before)
        {
            foreach(var day in Days.Keys)
            {
                if(day < before)
                {
                    Days.Remove(day);
                }
            }
        }
    }

    public class DaySchedule
    {
        [BsonElement("slots")]
        public HashSet<TimeSlot> Slots { get; set; }

        public DaySchedule(HashSet<TimeSlot> working_hours)
        {
            Slots = working_hours;
        }
    }
}