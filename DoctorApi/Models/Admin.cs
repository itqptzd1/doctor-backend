using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DoctorApi.Models.Users
{
    public class Admin : RawUser, User
    {
        [BsonElement("role")]
        public override string Role { get => Roles.Admin; }

        [BsonElement("name")]
        public PersonsName Name { get; set; }
    }
}
