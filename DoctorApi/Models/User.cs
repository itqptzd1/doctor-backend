using System.Collections.Generic;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DoctorApi.Models.Users
{
    ///<summary>
    ///Именованый список строк соответствующим имени каждой роли.
    ///</summary>
    public static class Roles
    {
        public const string Admin = "Admin";
        public const string Registry = "Registry";
        public const string Doctor = "Doctor";
        public const string Patient = "Patient";
    }

    ///<summary>
    ///Этот интерфейс объединяет все типы пользователей
    ///</summary>
    public interface User : DBObject
    {
        string Username { get; set; }

        string Password { get; set; }

        string Role { get; }

        string Token { get; set; }
    }

    ///<summary>
    ///Этот класс представляет пользователя, роль которого еще неизвестна.
    ///</summary>
    ///<remarks>
    ///Не рекомендуется использовать этот класс когда роль пользователя уже определена
    ///</remarks>
    [BsonDiscriminator(RootClass = true)]
    [BsonKnownTypes(typeof(Patient), typeof(Doctor), typeof(Registry), typeof(Admin))]
    public class RawUser : User, DBObject
    {
        [BsonId]
        [BsonIgnoreIfDefault]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("username")]
        public string Username { get; set; }

        [BsonElement("password")]
        public string Password { get; set; }

        public virtual string Role { get; }

        [BsonIgnore]
        public string Token { get; set; }
    }
}