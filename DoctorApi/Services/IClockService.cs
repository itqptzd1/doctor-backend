using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using Hangfire;
using NodaTime;
using NodaTime.TimeZones;
using DoctorApi.Helpers;
using DoctorApi.Models.Users;
using DoctorApi.Models.Time;

namespace DoctorApi.Services
{
    public interface IClockService
    {
        DateTimeZone TimeZone { get; }

        LocalDateTime? OffsetDateTime { get; }

        Instant Now { get; }

        LocalDateTime LocalNow { get; }

        Instant ToInstant(LocalDateTime local);

        LocalDateTime ToLocal(Instant instant);
    }

    public class SystemClockService : IClockService
    {
        public DateTimeZone TimeZone { get; }

        public LocalDateTime? OffsetDateTime { get; }

        public Instant Now
        {
            get
            {
                if(OffsetDateTime == null){
                    return SystemClock.Instance.GetCurrentInstant();
                }
                return OffsetDateTime.Value.InZoneLeniently(TimeZone).ToInstant();
            }
        }

        public LocalDateTime LocalNow 
        {
            get{
                return Now.InZone(TimeZone).LocalDateTime;
            }
        }

        public Instant ToInstant(LocalDateTime local)
        {
            return local.InZone(TimeZone, Resolvers.LenientResolver).ToInstant();
        }

        public LocalDateTime ToLocal(Instant instant)
        {
            return instant.InZone(TimeZone).LocalDateTime;
        }
    }
}