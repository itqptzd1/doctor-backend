using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
// using Hangfire;
using NodaTime;
using DoctorApi.Helpers;
using DoctorApi.Models.Users;
using DoctorApi.Models.Time;

namespace DoctorApi.Services
{
    public class DoctorService : CRUD<Doctor>, ICRUD<Doctor>
    {
        private readonly ILogger<DoctorService> _logger;
        private readonly IClockService _clock;

        public DoctorService(IDatabaseSettings settings, IClockService clockService, ILogger<DoctorService> logger) : base(settings, "Users")
        {
            _logger = logger;
            _clock = clockService;

            // TODO: configure hangfire
            // RecurringJob.AddOrUpdate(() => removeOldEntries(), "0 0 0 * * ?");  // Repeat each day at 12am
        }

        public bool AddAppointment(LocalDate day, Appointment appointment)
        {
            var doc = Get(appointment.Doctor.Id.AsString);

            if(doc == null) return false;

            if(doc.Schedule.addSlot(day, appointment.Time))
            {
                Update(doc.Id, doc);
                // TODO: Save appointment
                return true;
            }
            return false;
        }

        public void Tie(Doctor doctor, string deptId)
        {
            doctor.Department = new MongoDBRef("Departments", deptId);
            Update(doctor.Id, doctor);
        }

        public void removeOldEntries()
        {
            var doctors = Get();

            foreach(var doc in doctors)
            {
                doc.Schedule.removeOld(before: _clock.LocalNow.Date);
                Update(doc.Id, doc);
            }
        }

        // CRUD operations

        public override List<Doctor> Get() =>
            _collection.Find(doctor => doctor.Role == Roles.Doctor).ToList();
    }
}
