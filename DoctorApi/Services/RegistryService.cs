using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using DoctorApi.Helpers;
using DoctorApi.Models.Users;

namespace DoctorApi.Services
{
    public class RegistryService : CRUD<Registry>, ICRUD<Registry>
    {
        private readonly ILogger<RegistryService> _logger;

        public RegistryService(IDatabaseSettings settings, ILogger<RegistryService> logger) : base(settings, "Users")
        {
            _logger = logger;
        }

        public override List<Registry> Get() =>
            _collection.Find(patient => patient.Role == Roles.Registry).ToList();
    }
}
