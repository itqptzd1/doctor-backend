using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using DoctorApi.Helpers;
using DoctorApi.Models.Users;

namespace DoctorApi.Services
{
    public class PatientService : CRUD<Patient>, ICRUD<Patient>
    {
        private readonly ILogger<PatientService> _logger;

        public PatientService(IDatabaseSettings settings, ILogger<PatientService> logger) : base(settings, "Users")
        {
            _logger = logger;
        }

        public override List<Patient> Get() =>
            _collection.Find(patient => patient.Role == Roles.Patient).ToList();
    }
}
