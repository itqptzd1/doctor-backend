using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using DoctorApi.Helpers;
using DoctorApi.Models.Users;

namespace DoctorApi.Services
{
    public class AdminService : CRUD<Admin>, ICRUD<Admin>
    {
        private readonly ILogger<AdminService> _logger;

        public AdminService(IDatabaseSettings settings, ILogger<AdminService> logger) : base(settings, "Users")
        {
            _logger = logger;
        }

        public override List<Admin> Get() =>
            _collection.Find(patient => patient.Role == Roles.Admin).ToList();
    }
}
