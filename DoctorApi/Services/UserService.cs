using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using MongoDB.Driver;
using DoctorApi.Models.Users;
using DoctorApi.Helpers;

namespace DoctorApi.Services
{
    // TODO: Dont store passwords plaintext
    public class UserService
    {
        private readonly IMongoCollection<RawUser> _users;

        private readonly AppSettings _appSettings;
        private readonly ILogger _logger;

        public UserService(IDatabaseSettings DbSettings, IOptions<AppSettings> appSettings, ILogger<UserService> logger)
        {
            _appSettings = appSettings.Value;
            _logger = logger;

            var client = new MongoClient(DbSettings.ConnectionString);
            var database = client.GetDatabase(DbSettings.DatabaseName);

            _users = database.GetCollection<RawUser>("Users");
        }

        public RawUser Authenticate(string username, string password)
        {
            var user = _users.Find<RawUser>(x => x.Username == username && x.Password == password).SingleOrDefault();

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] 
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.Role)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            // remove password before returning
            user.Password = null;

            return user;
        }

        public User Register(RawUser user)
        {
            _users.InsertOne(user);
            return user;
        }
    }
}