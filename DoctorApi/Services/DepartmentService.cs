using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using DoctorApi.Helpers;
using DoctorApi.Models;

namespace DoctorApi.Services
{
    public class DepartmentService : CRUD<Department>, ICRUD<Department>
    {
        private readonly ILogger<DepartmentService> _logger;

        public DepartmentService(IDatabaseSettings settings, ILogger<DepartmentService> logger) : base(settings, "Departments")
        {
            _logger = logger;
        }
      
        public void Tie(Department department, string docId)
        {
            department.Doctors.Add(new MongoDBRef("Users", docId));
            Update(department.Id, department);
        }
    }
}
