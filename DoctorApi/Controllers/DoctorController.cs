﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json.Linq;
using NodaTime;
using DoctorApi.Models.Users;
using DoctorApi.Models.Time;
using DoctorApi.Services;

namespace DoctorApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DoctorController : CRUDController<Doctor>
    {
        private new DoctorService _mainService;
        private readonly DepartmentService _deptService;

        public DoctorController(DoctorService doctorService, DepartmentService departmentService) : base(doctorService)
        {
            _mainService = doctorService;
            _deptService = departmentService;
        }

        [HttpGet("schedule")]
        public ActionResult<List<TimeSlot> > GetSchedule(string id, LocalDate day)
        {
            var doc = _mainService.Get(id);
            return doc.Schedule.Days[day].Slots.ToList();
        }

        [Authorize(Roles = Roles.Registry + "," + Roles.Admin)] // TODO: Policy check
        [HttpPatch("{id:length(24)}/setDepartment")]
        public IActionResult SetDepartment(string id, [FromBody] string deptId)
        {
            var doc = _mainService.Get(id);
            var dept = _deptService.Get(deptId);

            if(doc == null) return BadRequest("Doctor with this ID doesn't exist");
            if(dept == null) return BadRequest("Department with this ID desn't exist");

            _mainService.Tie(doc, dept.Id);
            _deptService.Tie(dept, doc.Id);

            return Ok();
        }

        //******** CRUD methods ********// 

        // GET api/doctor/all
        [HttpGet("all")]
        public override ActionResult<List<Doctor> > GetAll() =>
            base.GetAll();

        // GET api/doctor/{id}
        
        [HttpGet("{id:length(24)}", Name = "GetDoctor")]
        public override ActionResult<Doctor> Get(string id) =>
            base.Get(id);

        // POST api/doctor/add
        [Authorize(Roles = Roles.Admin)]
        [HttpPost("add")]
        public override ActionResult<Doctor> Create(Doctor doc) =>
            base.Create(doc);

        // PUT api/doctor/{id}
        [Authorize(Roles = Roles.Registry + "," + Roles.Admin)]
        [HttpPut("{id:length(24)}")]
        public override IActionResult Update(string id, Doctor docNew) =>
            base.Update(id, docNew);

        // DELETE api/doctor/{id}
        [Authorize(Roles = Roles.Admin)]
        [HttpDelete("{id:length(24)}")]
        public override IActionResult Delete(string id) =>
            base.Delete(id);
    }
}
