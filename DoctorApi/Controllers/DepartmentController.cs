using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MongoDB.Driver;
using MongoDB.Bson;
using DoctorApi.Models;
using DoctorApi.Models.Users;
using DoctorApi.Services;

namespace DoctorApi.Controllers
{
    [Authorize(Roles = Roles.Admin)]
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentController : CRUDController<Department>
    {
        private readonly DoctorService _doctorService;

        public DepartmentController(DepartmentService dps, DoctorService dos) : base(dps)
        {
            _doctorService = dos;
        }

        // GET api/department/all
        [AllowAnonymous]
        [HttpGet("all")]
        public override ActionResult<List<Department> > GetAll() =>
            base.GetAll();

        // GET api/department/{id}
        [AllowAnonymous]
        [HttpGet("{id:length(24)}", Name = "GetDepartment")]
        public override ActionResult<Department> Get(string id) =>
            base.Get(id);

        // POST api/department/add
        [HttpPost("add")]
        public override ActionResult<Department> Create(Department dept) =>
            base.Create(dept);

        // PUT api/department/{id}
        [HttpPut("{id:length(24)}")]
        public override IActionResult Update(string id, Department deptNew) =>
            base.Update(id, deptNew);

        // DELETE api/department/{id}
        [HttpDelete("{id:length(24)}")]
        public override IActionResult Delete(string id) =>
            base.Delete(id);
    }
}
