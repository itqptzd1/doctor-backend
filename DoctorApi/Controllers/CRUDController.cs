using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using DoctorApi.Models;
using DoctorApi.Services;

namespace DoctorApi.Controllers
{
    [ApiController]
    public abstract class CRUDController<T> : ControllerBase where T : DBObject
    {
        protected readonly ICRUD<T> _mainService;

        public CRUDController(ICRUD<T> service)
        {
            _mainService = service;
        }

        // GET {base}/all
        [HttpGet("all")]
        public virtual ActionResult<List<T> > GetAll()
        {
            return _mainService.Get();
        }

        // GET {base}/{id}
        [HttpGet("{id:length(24)}", Name = "Get")]
        public virtual ActionResult<T> Get(string id)
        {
            var thing = _mainService.Get(id);

            if(thing == null) return NotFound();

            return thing;
        }

        // POST {base}/add
        [HttpPost("add")]
        public virtual ActionResult<T> Create(T thing)
        {
            _mainService.Create(thing);

            return CreatedAtRoute("Get", new {id = thing.Id.ToString() }, thing);
        }

        // PUT {base}/{id}
        [HttpPut("{id:length(24)}")]
        public virtual IActionResult Update(string id, [FromBody] T newThing)
        {
            var thing = _mainService.Get(id);

            if(thing == null) return NotFound();

            _mainService.Update(id, newThing);

            return NoContent();
        }

        // DELETE {base}/{id}
        [HttpDelete("{id:length(24)}")]
        public virtual IActionResult Delete(string id)
        {
            var thing = _mainService.Get(id);

            if(thing == null) return NotFound();

            _mainService.Remove(thing.Id);

            return NoContent();
        }

    }
}