using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using DoctorApi.Models.Users;
using DoctorApi.Services;

namespace DoctorApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PatientController : CRUDController<Patient>
    {
        public PatientController(PatientService service) : base(service)
        {
            // Nothing yet
        }

        // GET api/patient/all
        [HttpGet("all")]
        public override ActionResult<List<Patient> > GetAll() =>
            base.GetAll();

        // GET api/patient/{id}
        [HttpGet("{id:length(24)}", Name = "GetPatient")]
        public override ActionResult<Patient> Get(string id) =>
            base.Get(id);

        // Note: use register
        // POST api/patient/add
        [Authorize(Roles = Roles.Admin)]
        [HttpPost("add")]
        public override ActionResult<Patient> Create(Patient patient) =>
            base.Create(patient);

        // TODO: Allow registry through policy
        // PUT api/patient/{id}
        [Authorize(Roles = Roles.Admin)]
        [HttpPut("{id:length(24)}")]
        public override IActionResult Update(string id, Patient patientNew) =>
            base.Update(id, patientNew);

        // DELETE api/patient/{id}
        [Authorize(Roles = Roles.Admin)]
        [HttpDelete("{id:length(24)}")]
        public override IActionResult Delete(string id) =>
            base.Delete(id);

    }
}
