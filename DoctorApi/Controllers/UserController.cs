using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using DoctorApi.Services;
using DoctorApi.Models.Users;

namespace DoctorApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private UserService _userService;
        private PatientService _patientService;
        private DoctorService _doctorService;
        private ILogger _logger;

        public UsersController(UserService userService, PatientService patientService, DoctorService doctorService, ILogger<UsersController> logger)
        {
            _userService = userService;
            _patientService = patientService;
            _doctorService = doctorService;
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody] RawUser userParam)
        {
            var user = _userService.Authenticate(userParam.Username, userParam.Password);

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(user);
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult Register([FromBody] Patient userParam)
        {
            var user = _userService.Register(userParam);

            return Ok();
        }

        // Elevated registry allowing any role to be added
        [Authorize(Roles = Roles.Admin)]
        [HttpPost("add")]
        public IActionResult Add([FromBody] RawUser userParam)
        {
            var user = _userService.Register(userParam);

            return Ok();
        }
    }
}