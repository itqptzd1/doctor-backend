# Бэкенд-API сайта больницы

## Запуск

1. Перейти в директорию `DoctorApi`
2. `dotnet run`

## Зависимости

1. [ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/?view=aspnetcore-3.0)
2. [MongoDB.Driver](https://docs.mongodb.com/ecosystem/drivers/csharp/)
   - [Reference](https://mongodb.github.io/mongo-csharp-driver/)
3. [NodaTime](https://nodatime.org)
   - [API Reference](https://nodatime.org/2.4.x/api/NodaTime.html)
4. [HangFire](https://www.hangfire.io)
   - [Documentation](https://docs.hangfire.io/en/latest/)

Установить все зависимости можно либо вызвав `dotnet restore`, либо запустив `dotnet build` первый раз.

## Другая документация

- [От Bitbucket](doc/Bitbucket.md)
- [Raw API XML](DoctorApi/bin/Debug/netcoreapp2.2/DoctorApi.xml)
  - Доступно после компиляции
