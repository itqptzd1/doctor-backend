using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Doctor.Services.User.API.Models;
using Doctor.Services.User.API.Helpers;
using Doctor.Services.User.API.Models.ViewModels;
using Serilog;

namespace Doctor.Services.User.API.Services
{
    // TODO: Dont store passwords plaintext
    public class UserService
    {
        private readonly UserManager<ApplicationUser> _userManager;

        private readonly ITokenManager _tokenManager;

        public UserService(UserManager<ApplicationUser> userManager, IOptions<TokenManager> tokenManager)
        {
            _userManager = userManager;
            _tokenManager = tokenManager.Value;
            Log.Debug("UserService initialization token parametres {@Token}", _tokenManager);
        }

        public async Task<ApplicationUser> Authenticate(AuthenticationViewModel model)
        {
            Log.Verbose("Login attempt {@Model}", model);
            var user = await FindByUsername(model.Username);

            Log.Debug("Got user from db {@User}", user);

            if(await ValidateCredentials(user, model.Password))
            {
                Log.Information("Valid user logged in {User}", user);
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_tokenManager.Secret);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[] 
                    {
                        new Claim(ClaimTypes.Name, user.Id.ToString()),
                        new Claim(ClaimTypes.Role, user.Role.Value)
                    }),
                    Expires = DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                }?? throw new NullReferenceException("Could not construct a token descriptor");
                var token = tokenHandler.CreateToken(tokenDescriptor);
                user.Token = tokenHandler.WriteToken(token);

                return user;
            }
            else
            {
                return null;
            }
        }

        public async Task<ApplicationUser> Register(RegisterViewModel model)
        {
            var user = new ApplicationUser() { UserName = model.Username, Role = Roles.Patient };
            IdentityResult result = await _userManager.CreateAsync(user, model.Password);
            if(result.Succeeded)
            {
                Log.Information("New user registered {User}", user);
                return user;
            }
            return null;
        }

        private SecurityToken GenerateJwtToken(ApplicationUser user)
        {
            // authentication successful so generate jwt token
            var key = Encoding.ASCII.GetBytes(_tokenManager.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] 
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.Role.Value)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            return new JwtSecurityTokenHandler().CreateToken(tokenDescriptor);
        }

        private async Task<ApplicationUser> FindByUsername(string user)
        {
            return await _userManager.FindByNameAsync(user);
        }


        private async Task<bool> ValidateCredentials(ApplicationUser user, string password)
        {
            return await _userManager.CheckPasswordAsync(user, password);
        }

    }
}