using AspNetCore.Identity.MongoDbCore.Models;
using MongoDB.Bson.Serialization.Attributes;
using MongoDbGenericRepository.Attributes;

namespace Doctor.Services.User.API.Models
{
    [CollectionName("Users")]
    public class ApplicationUser : MongoIdentityUser<string>
    {
        [BsonElement("role")]
        public Roles Role { get; set; }

        [BsonIgnore]
        public string Token { get; set; }
    }
}