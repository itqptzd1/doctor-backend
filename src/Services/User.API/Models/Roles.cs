using System;

namespace Doctor.Services.User.API.Models
{
    public class Roles : IEquatable<Roles>
    {
        private Roles(string value) { Value = value; }

        public string Value { get; set; }

        public virtual bool Equals(Roles role)
        {
            if(role != null)
            {
                return Value.Equals(role.Value);
            }
            else
            {
                return false;
            }
        }

        // public static bool Equals(object objA, object objB)
        // {
        //     var role1 = objA as Roles;
        //     var role2 = objB as Roles;
        //     if(role1 != null && role2 != null)
        //     {
        //         return role1.Equals(role2);
        //     }
        //     else
        //     {
        //         return role1 == null && role2 == null;
        //     }
        // }

        public override bool Equals(object obj)
        {
            Roles role = obj as Roles;
            if(role != null)
            {
                return Equals(role);
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }

        public static Roles Admin { get { return new Roles("Admin"); } }
        public static Roles Registry { get { return new Roles("Registry"); } }
        public static Roles Doctor { get { return new Roles("Doctor"); } }
        public static Roles Patient { get { return new Roles("Patient"); } }
    }
}