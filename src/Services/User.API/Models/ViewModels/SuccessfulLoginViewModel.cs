using System.ComponentModel.DataAnnotations;

namespace Doctor.Services.User.API.Models.ViewModels
{
    public class SuccessfulLoginViewModel
    {
        public string Username { get; set; }
        public string Role { get; set; }
        public string Email { get; set; }

        public SuccessfulLoginViewModel(ApplicationUser user)
        {
            Username = user.UserName;
            Role = user.Role.Value;
            Email = user.Email;
        }
    }
}