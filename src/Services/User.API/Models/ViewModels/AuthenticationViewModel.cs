using System.ComponentModel.DataAnnotations;

namespace Doctor.Services.User.API.Models.ViewModels
{
    public class AuthenticationViewModel
    {
        [Required()]
        public string Username { get; set; }

        [Required()]
        public string Password { get; set; }
    }
}