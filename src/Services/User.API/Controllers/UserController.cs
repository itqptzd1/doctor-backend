using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using Doctor.Services.User.API.Services;
using Doctor.Services.User.API.Models;
using Doctor.Services.User.API.Models.ViewModels;
using Serilog;

namespace Doctor.Services.User.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private UserService _userService;

        public UsersController(UserService userService, ILogger<UsersController> logger)
        {
            _userService = userService;
        }

        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] AuthenticationViewModel model)
        {
            var user = await _userService.Authenticate(model);

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(new SuccessfulLoginViewModel(user));
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterViewModel model)
        {
            var user = await _userService.Register(model);

            if(user == null)
                return BadRequest(new { message = "Could not create user" });

            return Ok();
        }
    }
}