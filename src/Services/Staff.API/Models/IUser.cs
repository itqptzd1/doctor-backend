using System.Collections.Generic;

namespace Doctor.Services.Staff.API.Models
{
    public static class Roles
    {
        public const string Admin = "Admin";
        public const string Registry = "Registry";
        public const string Doctor = "Doctor";
        public const string Patient = "Patient";
    }

    public interface IUser
    {
        string Id { get; set; }
        
        string Username { get; set; }

        string Password { get; set; }

        string Role { get; }

        string Token { get; set; }
    }
}