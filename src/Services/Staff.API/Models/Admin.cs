using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Doctor.Services.Staff.API.Models
{
    public class Admin : IUser
    {
        [BsonId]
        [BsonIgnoreIfDefault]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("username")]
        public string Username { get; set; }

        [BsonElement("password")]
        public string Password { get; set; }

        [BsonElement("role")]
        public string Role { get => Roles.Admin; }

        [BsonElement("name")]
        public FullName Name { get; set; }

        [BsonIgnore]
        public string Token { get; set; }
    }
}
