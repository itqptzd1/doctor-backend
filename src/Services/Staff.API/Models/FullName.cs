using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Doctor.Services.Staff.API.Models
{
    public class FullName
    {
        [BsonElement("firstName")]
        public string FirstName { get; set; }
        [BsonElement("surname")]
        public string Surname { get; set; }
        [BsonElement("patronym")]
        public string Patronym { get; set; }
    }
}