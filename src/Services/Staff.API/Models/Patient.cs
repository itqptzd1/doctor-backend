using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Doctor.Services.Staff.API.Models
{
    public class Patient : IUser
    {
        [BsonId]
        [BsonIgnoreIfDefault]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("username")]
        public string Username { get; set; }

        [BsonElement("password")]
        public string Password { get; set; }

        [BsonElement("role")]
        public string Role { get => Roles.Patient; }

        [BsonElement("name")]
        public FullName Name { get; set; }

        // [BsonElement("med_id")]
        // public string MedId { get; set; }

        [BsonIgnore]
        public string Token { get; set; }
    }
}
