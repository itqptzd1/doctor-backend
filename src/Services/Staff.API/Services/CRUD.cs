using MongoDB.Driver;
using System.Collections.Generic;
using Doctor.Services.Staff.API.Helpers;
using Doctor.Services.Staff.API.Models;

namespace Doctor.Services.Staff.API.Services
{
    public interface ICRUD<T>
    {
        List<T> Get();

        T Get(string id);

        T Create(T thing);

        void Update(string id, T thing);

        void Remove(string id);

        void Remove(T thing);

    }

    public abstract class CRUD<T> : ICRUD<T> where T : IUser
    {
        protected IMongoCollection<T> _collection;

        public CRUD(IDatabaseSettings settings, string collectionName)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _collection = database.GetCollection<T>(collectionName);
        }

        public virtual List<T> Get() =>
            // TODO: better type checking
            _collection.Find(t => t.Role == typeof(T).ToString()).ToList();

        public virtual T Get(string id) =>
            _collection.Find(t => t.Id == id).FirstOrDefault();

        public virtual T Create(T thing)
        {
            _collection.InsertOne(thing);
            return thing;
        }

        public virtual void Update(string id, T thing) =>
            _collection.FindOneAndReplace(t => t.Id == id, thing);

        public virtual void Remove(string id) =>
            _collection.DeleteOne(t => t.Id == id);

        public virtual void Remove(T thing) =>
            _collection.DeleteOne(t => t.Id == thing.Id);
    }
}