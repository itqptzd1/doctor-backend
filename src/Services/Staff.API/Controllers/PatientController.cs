using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Doctor.Services.Staff.API.Models;
using Doctor.Services.Staff.API.Services;

namespace Doctor.Services.Staff.API.Controllers
{
    [Authorize]
    [Route("api/staff/[controller]")]
    [ApiController]
    public class PatientController : CRUDController<Patient>
    {
        public PatientController(CRUD<Patient> service, ILogger<CRUDController<Patient> > logger) : base(service, logger)
        {
            // Nothing yet
        }

        // GET api/staff/patient/all
        [HttpGet("all")]
        public override ActionResult<List<Patient> > GetAll() =>
            base.GetAll();

        // GET api/staff/patient/{id}
        [HttpGet("{id:length(24)}", Name = "GetPatient")]
        public override ActionResult<Patient> Get(string id) =>
            base.Get(id);

        // Note: use register
        // POST api/staff/patient/add
        [Authorize(Roles = Roles.Admin)]
        [HttpPost("add")]
        public override ActionResult<Patient> Create(Patient patient) =>
            base.Create(patient);

        // TODO: Allow registry through policy
        // PUT api/staff/patient/{id}
        [Authorize(Roles = Roles.Admin)]
        [HttpPut("{id:length(24)}")]
        public override IActionResult Update(string id, Patient patientNew) =>
            base.Update(id, patientNew);

        // DELETE api/staff/patient/{id}
        [Authorize(Roles = Roles.Admin)]
        [HttpDelete("{id:length(24)}")]
        public override IActionResult Delete(string id) =>
            base.Delete(id);

    }
}
