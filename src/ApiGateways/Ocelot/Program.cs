﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Ocelot.DependencyInjection;
using Serilog;

namespace ApiGw.Ocelot
{
    public class Program
    {
        public static readonly string Namespace = typeof(Program).Namespace;
        public static readonly string AppName = Namespace.Substring(Namespace.LastIndexOf('.', Namespace.LastIndexOf('.') - 1) + 1);

        public static int Main(string[] args)
        {
            Log.Logger = CreateSerilogLogger();

            var builder = PrepareWebHost(args);

            try
            {
                Log.Information("Configuring web host ({ApplicationContext})...", AppName);
                var host = builder.Build();

                Log.Information("Starting web host ({ApplicationContext})...", AppName);
                host.Run();

                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Program terminated unexpectedly ({ApplicationContext})!", AppName);
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static IWebHostBuilder PrepareWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseContentRoot(Directory.GetCurrentDirectory())
                .CaptureStartupErrors(false)
                .ConfigureAppConfiguration((hostingContext, config) => { config = CreateConfigurationBuilder(hostingContext); })
                .UseStartup<Startup>()
                .UseSerilog();
        

        private static Serilog.ILogger CreateSerilogLogger()
        {
            // var seqServerUrl = configuration["Serilog:SeqServerUrl"];
            var envDir = Directory.GetCurrentDirectory();
            var envName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            var configuration = new ConfigurationBuilder()
                .SetBasePath(envDir)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{envName}.json", optional: true, reloadOnChange: true)
                .Build();

            return new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .Enrich.WithProperty("ApplicationContext", AppName)
                .CreateLogger();
        }

        private static IConfigurationBuilder CreateConfigurationBuilder(WebHostBuilderContext hostingContext)
        {
            var env = hostingContext.HostingEnvironment;
            var envDir = Directory.GetCurrentDirectory();
            var envName = env.EnvironmentName;

            return new ConfigurationBuilder()
                .SetBasePath(envDir)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{envName}.json", optional: true, reloadOnChange: true)
                .AddJsonFile("ocelot.json")
                .AddJsonFile($"configuration.{envName}.json")
                .AddEnvironmentVariables();
        }

    }
}
